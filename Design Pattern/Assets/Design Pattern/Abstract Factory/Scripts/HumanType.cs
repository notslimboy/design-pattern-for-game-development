﻿
namespace AbstracFactory
{

    public enum HumanType
    {
        Beggar,
        ShopOwner,
        Farmer
    }
}
