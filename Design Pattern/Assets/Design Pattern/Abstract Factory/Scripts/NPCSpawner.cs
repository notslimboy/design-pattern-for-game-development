﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace AbstracFactory
{
    public class NPCSpawner : MonoBehaviour
    {
        private IAnimal dog;
        private IAnimal cat;
        private IAnimal lion;

        private IHuman beggar;
        private IHuman farmer;
        private IHuman shopOwner;

        private AbstractFactory abstractFactory;

        
        public void SpawnAnimals()
        {
            abstractFactory = FactoryProducer.GetAbstractFactory(FactoryType.Animal);
            dog = abstractFactory.GetAnimal(AnimalType.Dog);
            cat = abstractFactory.GetAnimal(AnimalType.Cat);
            lion = abstractFactory.GetAnimal(AnimalType.Lion);
            cat.Voice();
            dog.Voice();
            lion.Voice();
        }

        public void SpawnHumans()
        {
            abstractFactory = FactoryProducer.GetAbstractFactory(FactoryType.Human);
            beggar = abstractFactory.GetHuman(HumanType.Beggar);
            farmer = abstractFactory.GetHuman(HumanType.Farmer);
            shopOwner = abstractFactory.GetHuman(HumanType.ShopOwner);

            beggar.Speak();
            farmer.Speak();
            shopOwner.Speak();
        }
    }
}

