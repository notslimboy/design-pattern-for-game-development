﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace AbstracFactory
{
    public interface IHuman
    {
        void Speak();
    }
}
