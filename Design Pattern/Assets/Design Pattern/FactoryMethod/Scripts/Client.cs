﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace FactoryMethod
{
    public class Client : MonoBehaviour
    {
        public NPCSpawner NPCSpawner;
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.S))
            {
                NPCSpawner.SpawnVillagers();
            }
        }
    }
}

