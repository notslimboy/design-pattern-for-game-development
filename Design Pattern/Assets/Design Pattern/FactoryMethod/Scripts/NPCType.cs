﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace FactoryMethod
{
    public enum NPCType
    {

        Beggar,
        Farmer,
        ShopOwner
    }
}

