﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace FactoryMethod
{
    public class NPCSpawner : MonoBehaviour
    {
        public NPCFactory NPCFactory;
        private INPC farmer;
        private INPC shopOwner;
        private INPC beggar;


        public void SpawnVillagers()
        {
            farmer = NPCFactory.GetNPC(NPCType.Farmer);
            shopOwner = NPCFactory.GetNPC(NPCType.ShopOwner);
            beggar = NPCFactory.GetNPC(NPCType.Beggar);

            farmer.Speak();
            shopOwner.Speak();
            beggar.Speak();
        }
    }

}
