﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace AbstracFactory
{
    public class AnimalFactory : AbstractFactory
    {
        public override IAnimal GetAnimal(AnimalType animalType)
        {
            switch (animalType)
            {
                case AnimalType.Lion:
                    IAnimal lion = new Lion();
                    return lion;
                case AnimalType.Dog:
                    IAnimal dog = new Dog();
                    return dog;
                case AnimalType.Cat:
                    IAnimal cat = new Cat();
                    return cat;
            }
            return null;
        }

        public override IHuman GetHuman(HumanType humanType)
        {
            return null;
        }
    }
}

