﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace AbstracFactory
{
    public class Beggar : IHuman
    {
        public void Speak ()
        {
            Debug.Log("Beggar : Give me Some Food Please");
        }
    }
}

