﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace AbstracFactory
{
    public class FactoryProducer : MonoBehaviour
    {
        public static AbstractFactory GetAbstractFactory(FactoryType factoryType)
        {
            switch(factoryType)
            {
                case FactoryType.Human:
                    AbstractFactory humanFactory = new HumanFactory();
                    return humanFactory;
                case FactoryType.Animal:
                    AbstractFactory animalFactory = new AnimalFactory();
                    return animalFactory;
            }
            return null;
        }


    }
}

