﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace FactoryMethod 
{
    public interface INPC
    {
        void Speak();
    }
}

