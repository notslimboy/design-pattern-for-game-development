﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace AbstracFactory
{
    public class Client : MonoBehaviour
    {
        public NPCSpawner spawner;


        private void Update()
        {
            if(Input.GetKeyDown(KeyCode.H))
            {
                spawner.SpawnHumans();
            }
            if (Input.GetKeyDown(KeyCode.A))
            {
                spawner.SpawnAnimals();
            }
        }
    }

}
