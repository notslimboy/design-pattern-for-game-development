﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AbstracFactory
{
    public class Cat : IAnimal
    {
        public void Voice()
        {
            Debug.Log("Cat: Meoww");
        }
    }
}