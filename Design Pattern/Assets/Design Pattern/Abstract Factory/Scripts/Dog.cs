﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AbstracFactory
{
    public class Dog : IAnimal
    {
        public void Voice()
        {
            Debug.Log("Dog: Woof");
        }
    }
}