﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace FactoryMethod
{
    public class NPCFactory : MonoBehaviour
    {
        public INPC GetNPC(NPCType npcType)
        {
            switch (npcType)
            {
                case NPCType.Beggar:
                    INPC beggar = new Beggar();
                    return beggar;
                case NPCType.Farmer:
                    INPC farmer = new Farmer();
                    return farmer;
                case NPCType.ShopOwner:
                    INPC shopOwner = new ShopOwner();
                    return shopOwner;
            }

            return null;
        }
    }
}

