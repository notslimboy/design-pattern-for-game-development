﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AbstracFactory
{
    public class Lion : IAnimal
    {
        public void Voice()
        {
            Debug.Log("Lion : Roar");
        }
    }
}