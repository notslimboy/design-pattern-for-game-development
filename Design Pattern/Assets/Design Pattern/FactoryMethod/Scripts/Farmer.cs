﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace FactoryMethod
{
    public class Farmer : INPC
    {
        public void Speak()
        {
            Debug.Log("You reap what you sow");
        }
    }

}
