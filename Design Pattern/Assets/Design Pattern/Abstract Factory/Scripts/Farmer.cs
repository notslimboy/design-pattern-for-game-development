﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace AbstracFactory
{
    public class Farmer : IHuman
    {
        public void Speak()
        {
            Debug.Log("Farmer: Give me some seed");
        }
    }
}

