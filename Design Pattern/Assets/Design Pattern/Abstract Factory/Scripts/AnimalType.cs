﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace AbstracFactory
{
    public enum AnimalType 
    {
         Cat, 
         Lion,
         Dog
    }

}
